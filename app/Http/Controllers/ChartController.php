<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;


class ChartController extends Controller
{
    public function dashboard(Request $request)
        {
                 $data2= DB::table('tag_headings')
                 ->select(
                   'tag_title',
                  'overall_view_count')
                 ->groupBy('tag_title')
                 ->get(); 
                $array2[] = ['TagName', 'Count'];
                // print_r($data);exit;
                 foreach($data2 as $key => $value)
                 {
                  $array2[] = [$value->tag_title, $value->overall_view_count];
                 }
           
                 //echo json_encode($array2);exit;
                 $data= array(
                    'datadetails' => json_encode($array2),
                 );
               //  print_r($data);exit;
                 return view('dashboard',$data);
        }
}
